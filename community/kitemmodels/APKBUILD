# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kitemmodels
pkgver=5.102.0
pkgrel=0
pkgdesc="Models for Qt Model/View system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only AND LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kitemmodels-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kdescendantsproxymodel_smoketest and kdescendantsproxymodeltest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kdescendantsproxymodel(_smoketest|test)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4072aee205cc7a2e09d5513ac24bc3bcadce2451a1aba4a9a4cd0a55138f9178d016e8b9f1c8c65d948c76c66ea44ea5f0422df56474699af15b604ea9584ac6  kitemmodels-5.102.0.tar.xz
"
