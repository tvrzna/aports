# Contributor: Eivind Uggedal <eu@eju.no>
# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker
pkgver=20.10.23
_cli_commit=715524332ff91d0f9ec5ab2ec95f051456ed1dba	# https://github.com/docker/cli/commits/v$pkgver
_moby_commit=6051f142912a5c06064e96b92de5e4e8f052131b   # https://github.com/moby/moby/commits/v$pkgver
pkgrel=0
pkgdesc="Pack, ship and run any application as a lightweight container"
url="https://www.docker.io/"
arch="all"
license="Apache-2.0"
depends="docker-engine docker-cli"
_engine_deps="ca-certificates containerd iptables ip6tables tini-static"
makedepends="go go-md2man btrfs-progs-dev bash linux-headers coreutils lvm2-dev libtool libseccomp-dev
	$_engine_deps"
install="$pkgname.pre-install"

# from https://github.com/moby/moby:	grep libnetwork vendor.conf
_libnetwork_commit=05b93e0d3a95952f70c113b0bc5bdb538d7afdd7
# from https://github.com/docker/cli:	grep cobra vendor.conf
_cobra_ver="1.1.1"

# secfixes:
#   20.10.20-r0:
#     - CVE-2022-39253
#   20.10.18-r0:
#     - CVE-2022-36109
#   20.10.16-r0:
#     - CVE-2022-29526
#   20.10.14-r0:
#     - CVE-2022-24769
#   20.10.11-r0:
#     - CVE-2021-41190
#   20.10.9-r0:
#     - CVE-2021-41089
#     - CVE-2021-41091
#     - CVE-2021-41092
#   20.10.3-r0:
#     - CVE-2021-21285
#     - CVE-2021-21284
#   19.03.14-r0:
#     - CVE-2020-15257
#   19.03.11-r0:
#     - CVE-2020-13401
#   19.03.1-r0:
#     - CVE-2019-14271
#   18.09.8-r0:
#     - CVE-2019-13509
#   18.09.7-r0:
#     - CVE-2018-15664

subpackages="
	$pkgname-engine:engine
	$pkgname-openrc:engine_openrc:noarch
	$pkgname-cli:cli
	$pkgname-doc:cli_doc:noarch
	$pkgname-bash-completion:cli_bashcomp:noarch
	$pkgname-fish-completion:cli_fishcomp:noarch
	$pkgname-zsh-completion:cli_zshcomp:noarch
	"

source="
	cli-$pkgver.tar.gz::https://github.com/docker/cli/archive/v$pkgver.tar.gz
	moby-$pkgver.tar.gz::https://github.com/moby/moby/archive/v$pkgver.tar.gz
	libnetwork-$_libnetwork_commit.tar.gz::https://github.com/docker/libnetwork/archive/$_libnetwork_commit.tar.gz
	cobra-$_cobra_ver.tar.gz::https://github.com/spf13/cobra/archive/v$_cobra_ver.tar.gz
	docker.initd
	docker.confd
	"

_cli_builddir="$srcdir/cli-$pkgver"
_moby_builddir="$srcdir/moby-$pkgver"
_libnetwork_builddir="$srcdir/libnetwork-$_libnetwork_commit"

_buildtags="seccomp"

export GO111MODULE=off # go1.16 defaults to on
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export AUTO_GOPATH=1
	export GITCOMMIT=$_cli_commit		# for cli
	export DOCKER_GITCOMMIT=$_moby_commit	# for moby
	export DOCKER_BUILDTAGS=$_buildtags
	export DISABLE_WARN_OUTSIDE_CONTAINER=1
	unset CC # prevent possible ccache issues

	case "$CARCH" in
		armv7) export GOARM=7;;
	esac

	# libnetwork (docker-proxy)
	msg "building docker-proxy"
	cd "$_libnetwork_builddir"
	mkdir -p src/github.com/docker/
	ln -sf "$_libnetwork_builddir" src/github.com/docker/libnetwork
	GOPATH="$PWD" go build -v -ldflags="-linkmode=external" -o docker-proxy github.com/docker/libnetwork/cmd/proxy

	# engine (moby)
	msg "building engine"
	cd "$_moby_builddir"
	mkdir -p src/github.com/docker/
	ln -sf "$_moby_builddir" src/github.com/docker/docker
	GOPATH="$PWD" VERSION="$pkgver" hack/make.sh dynbinary

	# Required for building man-pages
	export GOPATH="$_cli_builddir"
	export GOBIN="$GOPATH/bin"
	export PATH="$GOBIN:$PATH"
	# go-md2man package installs go-md2man, but this looks for md2man
	if ! command -v md2man >/dev/null 2>&1; then
		mkdir -p /tmp/bin
		ln -sf /usr/bin/go-md2man /tmp/bin/md2man
		export PATH="/tmp/bin:$PATH"
	fi

	# cli
	msg "building cli"
	cd "$_cli_builddir"
	mkdir -p "$GOPATH"/src/github.com/docker/
	ln -sf "$_cli_builddir" "$GOPATH"/src/github.com/docker/cli
	LDFLAGS="" make VERSION="$pkgver" dynbinary

	# docker man
	msg "building docker man pages"
	mkdir -p "$GOPATH"/src/github.com/spf13/
	ln -sf "$srcdir/cobra-$_cobra_ver" "$GOPATH"/src/github.com/spf13/cobra
	make manpages
}

# docker itself is a meta package
package() {
	mkdir -p "$pkgdir"
}

engine() {
	pkgdesc="Docker Engine (dockerd)"
	depends="$_engine_deps"

	install -Dm755 "$_moby_builddir"/bundles/dynbinary-daemon/dockerd \
		"$subpkgdir"/usr/bin/dockerd

	install -Dm755 "$_libnetwork_builddir"/docker-proxy \
		"$subpkgdir"/usr/bin/docker-proxy

	# symlink externally provided tini-static binary
	ln -sf /sbin/tini-static "$subpkgdir"/usr/bin/docker-init
}

engine_openrc() {
	pkgdesc="OpenRC init scripts for Docker"
	depends=""
	install_if="openrc $pkgname-engine=$pkgver-r$pkgrel"

	install -Dm755 "$srcdir"/docker.initd "$subpkgdir"/etc/init.d/docker
	install -Dm644 "$srcdir"/docker.confd "$subpkgdir"/etc/conf.d/docker
}

cli() {
	pkgdesc="Docker CLI"
	depends="ca-certificates"

	# 'build/docker' is a symlink to 'docker-linux-$arch' e.g. 'docker-linux-amd64'
	install -Dm755 "$_cli_builddir"/build/docker \
		"$subpkgdir"/usr/bin/docker
}

cli_doc() {
	pkgdesc="Documentation for Docker"
	depends=""
	install_if="docs $pkgname-cli=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"/usr/share/man/man1
	gzip -9 "$_cli_builddir"/man/man1/*
	install -Dm644 "$_cli_builddir"/man/man1/* \
		"$subpkgdir"/usr/share/man/man1
}

cli_bashcomp() {
	pkgdesc="Bash completion for Docker"
	depends=""
	install_if="bash-completion $pkgname-cli=$pkgver-r$pkgrel"

	install -Dm644 "$_cli_builddir"/contrib/completion/bash/$pkgname \
		"$subpkgdir"/usr/share/bash-completion/completions/$pkgname
}

cli_fishcomp() {
	pkgdesc="Fish shell completion for Docker"
	depends=""
	install_if="fish $pkgname-cli=$pkgver-r$pkgrel"

	install -Dm644 "$_cli_builddir"/contrib/completion/fish/$pkgname.fish \
		"$subpkgdir"/usr/share/fish/completions/$pkgname.fish
}

cli_zshcomp() {
	pkgdesc="Zsh completion for Docker"
	depends=""
	install_if="zsh $pkgname-cli=$pkgver-r$pkgrel"

	install -Dm644 "$_cli_builddir"/contrib/completion/zsh/_$pkgname \
		"$subpkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
c1debb583e9464d51ef482de29f63d6df1815eaae18cd66e5b4b36dee324f8c5c699134eee13ed4882e24aadaac37483a37e6729b734a60510271b107dc31cf0  cli-20.10.23.tar.gz
f6e13a275c7f368e90779b2bae4ded98c8949f398934fb249b10958973d86aaa3bc10a868fa64eb245e35ddb36ff14a89f6b20ff274e7e3b57a57daffa00c26d  moby-20.10.23.tar.gz
079eee720a4e755639e39fd8764f380549e715cbd6be0b46a102771a09f6cce5f085f7e568429c8c35a46f09948aea3e60de5ba2e32e22f0ee1fd9559c2d58f6  libnetwork-05b93e0d3a95952f70c113b0bc5bdb538d7afdd7.tar.gz
a27debc5c971f468e672826659e5c46946187e2307dded8c496084b9fabc5602e68bdfdc08f444f42d6f82f0f2704d01a0c4bde3e5fbe674c7cb14309f0a3328  cobra-1.1.1.tar.gz
3e913a856ce4452ff090764a3320f07b75163b17fa9b95544f74c1690214d416624a82f1e2bef897ef94ac5eda9dbfa961998c6cb9e2fd0aa5f65ff0afcdb426  docker.initd
f25523f43376ccef71a49618e556e0a16db3acad29eb09fe86c4e572562bdea0bc1eabab00159278835ad9d7c007f2cd10b2ed31f7213b0d9074582dc80a976f  docker.confd
"
