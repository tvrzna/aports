# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=stylua
pkgver=0.16.0
pkgrel=0
pkgdesc="Opinionated Lua code formatter"
url="https://github.com/JohnnyMorganz/StyLua"
arch="all !s390x !riscv64" # blocked by cargo
license="MPL-2.0"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://github.com/JohnnyMorganz/StyLua/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/StyLua-$pkgver"

_features="--features lua54"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen $_features
}

check() {
	cargo test --frozen $_features
}

package() {
	install -Dm755 target/release/stylua -t "$pkgdir"/usr/bin/
}

sha512sums="
77b8750c916862aef785b6d4ab5753a8e1772625478e8a5b58993e7d410408419a5ebabaff917c93df3a091d347c97c0526b757d18c5cf68899a9cb22874ec90  stylua-0.16.0.tar.gz
"
