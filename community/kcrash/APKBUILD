# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcrash
pkgver=5.102.0
pkgrel=0
pkgdesc="Support for application crash analysis and bug report from apps"
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcoreaddons-dev
	kwindowsystem-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcrash-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# kcrashtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kcrashtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
524fe2c0341dbe910b844bb44cd9779f799848f61f482eb435f298af895f16157d09ed3c5b7d1ed46c45b146971379149b6cf575bd02cc25f4545689dcd3029c  kcrash-5.102.0.tar.xz
"
