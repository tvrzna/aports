# Contributor: techknowlogick <techknowlogick@gitea.io>
# Maintainer: techknowlogick <techknowlogick@gitea.io>
pkgname=helm
pkgver=3.11.0
pkgrel=0
pkgdesc="The Kubernetes Package Manager"
url="https://helm.sh/"
arch="all"
license="Apache-2.0"
makedepends="bash go"
options="net"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/helm/helm/archive/v$pkgver.tar.gz"

# secfixes:
#   3.6.0-r0:
#     - CVE-2021-21303
#   3.6.1-r0:
#     - CVE-2021-32690

case "$CARCH" in
	# Disable check on 32bit systems due to upstream test "TestPlatformPrepareCommand" that does not account for these platforms
	x86|armv7|armhf) options="$options !check" ;;
esac

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

export GOFLAGS="$GOFLAGS -modcacherw"
export CGO_ENABLED=0

build() {
	export CGO_ENABLED=0 # breaks on aarch64
	make GOFLAGS="$GOFLAGS" GIT_TAG="v$pkgver" GIT_COMMIT="" GIT_DIRTY=""

	./bin/helm completion bash > $pkgname.bash
	./bin/helm completion fish > $pkgname.fish
	./bin/helm completion zsh > $pkgname.zsh
}

check() {
	make test-unit GOFLAGS="$GOFLAGS"
}

package() {
	install -Dm755 bin/helm -t "$pkgdir/usr/bin"

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
d8d3a6dcae225732e844f4d223de1c3b97f3c5d02a435225b038e9c46c37e07c07948adef0743ebd735fa3eed141406c57a68165fdb678742eb9e243b5e50349  helm-3.11.0.tar.gz
"
