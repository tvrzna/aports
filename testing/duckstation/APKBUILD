# Maintainer: psykose <alice@ayaya.dev>
pkgname=duckstation
pkgver=0_git20230120
pkgrel=0
_gitrev=9c6c3892e298c062bcd6e38458f66dbe690d5c2a
pkgdesc="Fast PlayStation 1 emulator"
url="https://github.com/stenzek/duckstation"
# supported+passing arches
arch="aarch64 x86_64 x86"
# there's more, find them later
license="GPL-3.0-or-later AND GPL-2.0-or-later AND Apache-2.0"
makedepends="
	clang
	cmake
	curl-dev
	extra-cmake-modules
	fmt-dev
	libdrm-dev
	libxrandr-dev
	lld
	llvm
	mesa-dev
	minizip-dev
	msgsl
	pulseaudio-dev
	qt6-qtbase-dev
	qt6-qttools-dev
	samurai
	sdl2-dev
	soundtouch-dev
	tinyxml2-dev
	vulkan-loader-dev
	wayland-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
subpackages="$pkgname-nogui"
source="$pkgname-$_gitrev.tar.gz::https://github.com/stenzek/duckstation/archive/$_gitrev.tar.gz
	system-deps.patch
	"
builddir="$srcdir/duckstation-$_gitrev"

build() {
	export CC=clang
	export CXX=clang++
	export CFLAGS="$CFLAGS -flto=thin"
	export CXXFLAGS="$CXXFLAGS -flto=thin -I/usr/include/minizip -I/usr/include/soundtouch"
	export LDFLAGS="$LDFLAGS -fuse-ld=lld"

	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DLAZY_LOAD_LIBS=OFF \
		-DUSE_WAYLAND=ON \
		-DUSE_DRMKMS=ON \
		-DBUILD_NOGUI_FRONTEND=ON \
		-DENABLE_DISCORD_PRESENCE=OFF
	cmake --build build
}

check() {
	./build/bin/common-tests
}

package() {
	mkdir -p "$pkgdir"/usr/lib/duckstation/

	rm build/bin/common-tests
	cp -a build/bin/* "$pkgdir"/usr/lib/duckstation/

	install -Dm755 /dev/stdin "$pkgdir"/usr/bin/duckstation-qt <<-EOF
		#!/bin/sh
		/usr/lib/duckstation/duckstation-qt "\$@"
	EOF
	install -Dm755 /dev/stdin "$pkgdir"/usr/bin/duckstation-nogui <<-EOF
		#!/bin/sh
		/usr/lib/duckstation/duckstation-nogui "\$@"
	EOF
}

nogui() {
	pkgdesc="$pkgdesc (nogui version)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/bin/duckstation-nogui
	amove usr/lib/duckstation/duckstation-nogui
}

sha512sums="
c6db214958a32994d1cf84fd268afb1e15c1e286f95fdb06e74d8eaba47f21c49c4a6ea1d7b85b97c7757ce139230fb5120c3d6ae4be3e2fd7453e6d197de511  duckstation-9c6c3892e298c062bcd6e38458f66dbe690d5c2a.tar.gz
58e2ade4d2165a5370b0394a1fff647c6ef7b5001a4be77e77f44c5ec5e698d960136b28089b8927d96b451e1258159dc79a9d8a692a1c32f972ca0c172d5452  system-deps.patch
"
