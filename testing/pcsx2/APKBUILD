# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=pcsx2
pkgver=1.7.3980
pkgrel=0
_fastfloat=6876616f0f6e69534b42ff0524fd616b56116736
_glslang=5755de46b07e4374c05fb1081f65f7ae1f8cca81
_gtest=58d77fa8070e8cec2dc1ed015d66b454c8d78850
_libchdr=464044e6cc5c7cfeb331906f4ef67d13dd4c83b6
_rcheevos=e99fa55b1cfeb665aab209cf0b3dd9970e9caa31
pkgdesc="Playstation 2 emulator"
url="https://github.com/PCSX2/pcsx2"
arch="x86_64" # only x86_64 supported for now
license="GPL-3.0-or-later AND LGPL-3.0-or-later"
# for some reason they added a bunch of dlopen spaghetti instead of linking to
# this. why?
depends="ffmpeg-libs"
makedepends="
	alsa-lib-dev
	clang
	cmake
	curl-dev
	eudev-dev
	ffmpeg-dev
	fmt-dev
	libaio-dev
	libpcap-dev
	libpng-dev
	libzip-dev
	lld
	llvm
	pulseaudio-dev
	qt6-qtbase-dev
	qt6-qttools-dev
	rapidyaml-dev
	samurai
	sdl2-dev
	soundtouch-dev
	vulkan-headers
	wayland-dev
	zstd-dev
	"
checkdepends="perl"
subpackages="$pkgname-doc"
source="https://github.com/PCSX2/pcsx2/archive/refs/tags/v$pkgver/pcsx2-v$pkgver.tar.gz
	fastfloat-$_fastfloat.tar.gz::https://github.com/fastfloat/fast_float/archive/$_fastfloat.tar.gz
	glslang-$_glslang.tar.gz::https://github.com/KhronosGroup/glslang/archive/$_glslang.tar.gz
	gtest-$_gtest.tar.gz::https://github.com/google/googletest/archive/$_gtest.tar.gz
	libchdr-$_libchdr.tar.gz::https://github.com/rtissera/libchdr/archive/$_libchdr.tar.gz
	rcheevos-$_rcheevos.tar.gz::https://github.com/RetroAchievements/rcheevos/archive/$_rcheevos.tar.gz
	fast-float.patch
	unbundle-vulkan.patch
	rapidyaml-0.15.patch
	version.patch

	PCSX2.desktop
	"

prepare() {
	default_prepare

	rmdir 3rdparty/glslang/glslang
	rmdir 3rdparty/gtest
	rmdir 3rdparty/libchdr/libchdr
	rmdir 3rdparty/rcheevos/rcheevos

	rm -r 3rdparty/ffmpeg

	ln -sfv "$srcdir"/glslang-$_glslang \
		"$builddir"/3rdparty/glslang/glslang
	ln -sfv "$srcdir"/googletest-$_gtest \
		"$builddir"/3rdparty/gtest
	ln -sfv "$srcdir"/libchdr-$_libchdr \
		"$builddir"/3rdparty/libchdr/libchdr
	ln -sfv "$srcdir"/rcheevos-$_rcheevos \
		"$builddir"/3rdparty/rcheevos/rcheevos

	ln -sfv "$srcdir"/fast_float-$_fastfloat \
		"$builddir"/3rdparty/fast_float

	sed -i \
		-e "s|@@VERSION@@|$pkgver|g" \
		-e "s|@@PKGREL@@|$pkgrel|g" \
		cmake/Pcsx2Utils.cmake
}

build() {
	export CC=clang
	export CXX=clang++
	export CFLAGS="$CFLAGS -flto=thin"
	export CXXFLAGS="$CXXFLAGS -flto=thin -Wno-macro-redefined"
	export LDFLAGS="$LDFLAGS -fuse-ld=lld -Wl,--icf=safe,--gc-sections"

	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DDISABLE_ADVANCE_SIMD=ON \
		-DDISABLE_BUILD_DATE=ON \
		-DENABLE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DCUBEB_API=ON \
		-DUSE_ACHIEVEMENTS=ON \
		-DUSE_DISCORD_PRESENCE=OFF \
		-DUSE_SYSTEM_FMT=ON \
		-DUSE_SYSTEM_RYML=ON \
		-DUSE_SYSTEM_ZSTD=ON \
		-DUSE_VTUNE=OFF \
		-DUSE_VULKAN=ON \
		-DQT_BUILD=ON \
		-DWAYLAND_API=ON \
		-DX11_API=ON

	cmake --build build --target all $(want_check && echo unittests)
}

check() {
	ctest --test-dir build/tests/ctest --output-on-failure
}

package() {
	install -Dm644 bin/resources/icons/AppIconLarge.png \
		"$pkgdir"/usr/share/icons/hicolor/256x256/apps/PCSX2.png
	install -Dm644 "$srcdir"/PCSX2.desktop \
		-t "$pkgdir"/usr/share/applications/

	install -Dm644 bin/docs/PCSX2.1 \
		-t "$pkgdir"/usr/share/man/man1/

	mkdir -p "$pkgdir"/usr/lib/

	rm build/bin/*test*
	cp -a build/bin "$pkgdir"/usr/lib/PCSX2

	install -Dm755 /dev/stdin "$pkgdir"/usr/bin/pcsx2 <<-EOF
		#!/bin/sh
		exec /usr/lib/PCSX2/pcsx2-qt "\$@"
	EOF
}

sha512sums="
c632c45bc8dec2923404f5cebc802abafddfcb9af1b15e5beb946e43b77b8b4689c80a00e07f42f25bad4d3748654009626ecef9ad46f358015744456230877f  pcsx2-v1.7.3980.tar.gz
f89a55c0a21c54bfefd307a122364fe066b265dcfca0b1315a97571fa199127bf0894f973d6cc5a550f7b88341ca778902f2ea5dd7e4c058af931a35df2c4b52  fastfloat-6876616f0f6e69534b42ff0524fd616b56116736.tar.gz
f307ec5990f2e501b39c43fb805e595fc8ce3ec7a6fff57ae51130dbc29101125b3b9d6b1de169f6710ada17f9a5301e8c524ca2c5a4ab9a45529733f048ab68  glslang-5755de46b07e4374c05fb1081f65f7ae1f8cca81.tar.gz
d8153c426e4f9c89a74721cc4a24dfcaf319810f4f10aa25fc972f99da2d96d66bc840cf2f51b756fef6b1ca47e8d2c8633f5862cc24e34d57309ad48802124a  gtest-58d77fa8070e8cec2dc1ed015d66b454c8d78850.tar.gz
367911b53b6087b488031ca67b39d3e095ba3a556ffe0e4d250a37a84e391ffdcd26ae30f59b94ba9a5af3810329baf22d101c62516aa5014e188f2823e80e4f  libchdr-464044e6cc5c7cfeb331906f4ef67d13dd4c83b6.tar.gz
f0f0b525e282b9cfd2d67793a5d2d2986742276c1a33ce0282f5f15a415a504783d2fd0af62b18d17cc5bba2d913918f9c4bf0d4cb4d33299b1f1dfc8453edb1  rcheevos-e99fa55b1cfeb665aab209cf0b3dd9970e9caa31.tar.gz
20624d3e46b956a5f0339b569891824b7a9321f4395c572393d42f7520d93b30b6e6bb8af90b5d6c52cbcaf2d82eb386bec3943532c5f1d8303c8b50938cd89b  fast-float.patch
94f8c4958da356e9f37465d75667f83ae1d2f7bace4a6f0c87a7ee177aa2c361ea06785369655d460182863ca7e08a3af7720415eaf041ae7fe250274a84ffb6  unbundle-vulkan.patch
0cd3bd5aa94b4642f7f9dc227a7b8a3ebddb5b9cea31eac0ca4f406fcc55077754de877e4549d641d01ed599a51772eb6a3b7b04959f71a95b9d75d7bafbe53f  rapidyaml-0.15.patch
113f0e80c7975985d65c90acd2b4c9a4487d0c9f240178500feac512808b836d6ae36343895a4ef4f16d91364eeee4f0a3aa4ecc17708facdaf0cb430b5e9a71  version.patch
bdc23c2773e1b963b06c0a51e3685b0a86db7dfe5cb2dd3e0cad4dd6f72aab2abef7eb421c8fb0f5303fae5c7ffb265eaa3a6e470998943aed54527bbb4d676c  PCSX2.desktop
"
