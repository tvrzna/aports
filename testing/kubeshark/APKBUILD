# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=kubeshark
pkgver=38.4
pkgrel=0
pkgdesc="API traffic viewer for Kubernetes"
url="https://kubeshark.co/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/kubeshark/kubeshark/archive/refs/tags/$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="
	-X github.com/kubeshark/kubeshark/misc.GitCommitHash=AlpineLinux
	-X github.com/kubeshark/kubeshark/misc.Branch=main
	-X github.com/kubeshark/kubeshark/misc.BuildTimestamp=$(date -u "+%s" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X github.com/kubeshark/kubeshark/misc.Platform=linux_$(go env GOARCH)
	-X github.com/kubeshark/kubeshark/misc.Ver=$pkgver
	"
	go build -v -o bin/$pkgname -ldflags "$_goldflags" kubeshark.go

	for shell in bash fish zsh; do
		./bin/$pkgname completion $shell > $pkgname.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
6fa0d2e3160eacf4960997864ce6771d520b0df8dcaaf2b004bc4ea7bc74cb3aec902d71736756cebe40ff35a510f204985295088bbc52d1e83fd0179204f5a3  kubeshark-38.4.tar.gz
"
